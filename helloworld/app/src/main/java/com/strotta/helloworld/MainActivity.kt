package com.strotta.helloworld

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    // Definicion de variable -> nombre del objeto : tipo del objeto
    // Si es una constante, en vez de var se usa "val"
    // Kotlin es nule safety, me ayuda a evitar definir cosas nulas. Lateinit significa que lo voy a inicializar despues
    lateinit var txtCartel : TextView

    lateinit var btnShow : Button

    // Ejemplo de definicion de texto. Si no quiero darle un valor inicial hay que usar lateinit
    var textoCartel : String = "Hola mundo!"
    // tambien podria definirlo como var text = "" . Saca por contexto el tipo de variale

    // pone la vista que saca de R.layout.activity. El onCreate se ejecuta una sola vez, cuando se crea
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Hago el binding, uno mi variable con el XML
        txtCartel = findViewById(R.id.txtCartel)
        btnShow = findViewById(R.id.btnShow)

    }

    // Se ejecuta cuando comienza
    override fun onStart() {
        super.onStart()

        // listener del boton (escucha evento)
        btnShow.setOnClickListener {
            txtCartel.text = textoCartel
        }
    }

}
